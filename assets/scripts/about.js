const main_page = document.getElementById("main");
const about_page = document.getElementById("about");
const project_page = document.getElementById("newProject");
const aboutNav_1 = document.getElementById("about_nav_1");
const aboutNav_2 = document.getElementById("about_nav_2");
const aboutNav_3 = document.getElementById("about_nav_3");
const projectNav_1 = document.getElementById("newProject_nav_1");
const projectNav_2 = document.getElementById("newProject_nav_2");
const projectNav_3 = document.getElementById("newProject_nav_3");

const projectNavOffsetHeight =
	project_page.querySelector(".parallax_left").offsetHeight;

// !offsetHeight START

const mainOffset = main_page.offsetHeight;
const aboutParallaxOffset = about_page.querySelector(".parallax").offsetHeight;
const aboutOffset = about_page.offsetHeight;
const projectParallaxOffset =
	project_page.querySelector(".parallax_left").offsetHeight;
const projectOffset = project_page.offsetHeight;
const project_offsetTop = project_page.offsetTop;
const projectPageTotalHeight =
	project_page.offsetHeight + project_page.offsetTop - about_page.offsetTop;

// !offsetHeight END

const parallax_fixed = "parallax_fixed";
const parallax_left_fixed = "parallax_left_fixed";
const invisible = "invisible";
const visible = "visible";
const displayNone = "displayNone";

//  !burger menu START
const magicNav = document.getElementById("magic_nav");
const title_H1 = magicNav.querySelector(".menu_about").querySelector("h1");
const burgerMenu = document.getElementsByClassName("burger_menu");
const menuOption = document.getElementById("menu_option");

function toggleBurger() {
	burgerMenu[0].classList.toggle("change");
	menuOption.classList.toggle("displayNone");
}

burgerMenu[0].addEventListener("click", () => {
	toggleBurger();
});

function resetBurgerMenuHandler() {
	burgerMenu[0].classList.remove("change");
	menuOption.classList.add("displayNone");
}
resetBurgerMenuHandler();
//  !burger menu END

// !about nav scrolling START
function aboutNavScrollingHandler() {
	if (window.scrollY <= about_page.offsetTop) {
		aboutNav_3.classList.remove(parallax_fixed);
		aboutNav_3.classList.add(invisible);
		aboutNav_1.classList.remove(invisible);
	}
	if (
		window.scrollY > about_page.offsetTop &&
		window.scrollY <= about_page.offsetHeight
	) {
		aboutNav_1.classList.add(invisible);
		aboutNav_3.classList.add(parallax_fixed);
		aboutNav_3.classList.remove(invisible);
	}
	if (window.scrollY <= about_page.offsetHeight) {
		aboutNav_2.classList.add(invisible);
	}
	if (window.scrollY > about_page.offsetHeight) {
		aboutNav_3.classList.add(invisible);
		aboutNav_2.classList.remove(invisible);
	}
}
aboutNavScrollingHandler();
// !about nav scrolling END

// !project nav scrolling START
function projectNavScrollingHandler() {
	if (window.scrollY <= project_page.offsetTop) {
		projectNav_1.classList.remove(invisible);
		projectNav_3.classList.remove(parallax_left_fixed);
	}
	if (
		window.scrollY > project_page.offsetTop &&
		window.scrollY <= projectPageTotalHeight
	) {
		projectNav_1.classList.add(invisible);
		projectNav_3.classList.add(parallax_left_fixed);
		projectNav_3.classList.remove(invisible);
	}
	if (window.scrollY <= projectPageTotalHeight) {
		projectNav_2.classList.add(invisible);
	}
	if (window.scrollY > projectPageTotalHeight) {
		projectNav_3.classList.add(invisible);
		projectNav_2.classList.remove(invisible);
	}
}
projectNavScrollingHandler();
// !project nav scrolling END

// !magic nav scrolling START
function magicNavScrollingHandler() {
	if (window.outerWidth >= 750) {
		magicNav.classList.add("displayNone");
	}
	if (window.outerWidth < 750) {
		if (window.scrollY < mainOffset + aboutParallaxOffset - 50) {
			magicNav.classList.add("displayNone");
		}
		if (window.scrollY >= mainOffset + aboutParallaxOffset - 50) {
			magicNav.classList.remove("displayNone");
		}
		// !above project
		if (
			window.scrollY <
			mainOffset + aboutOffset + projectNavOffsetHeight - magicNav.offsetHeight
		) {
			magicNav.classList.remove("aboveProject");
			title_H1.innerHTML = "About";
		}
		if (
			window.scrollY >=
			mainOffset + aboutOffset + projectNavOffsetHeight - magicNav.offsetHeight
		) {
			magicNav.classList.add("aboveProject");
			title_H1.innerHTML = "Project";
		}
	}
}
magicNavScrollingHandler();
// !magic nav scrolling END

function scrollHandler() {
	resetBurgerMenuHandler();
	// if (window.outerWidth < 1000) {
	// 	aboutNav_3.classList.add(displayNone);
	// 	aboutNav_2.classList.add(displayNone);
	// }
	if (window.outerWidth >= 1000) {
		// console.dir(main_page);
		// console.dir(about_page);
		// ! nav parallax
		// ? about nav
		aboutNavScrollingHandler();

		// ? project nav
		projectNavScrollingHandler();
	}

	// ! for window outer width <650
	magicNavScrollingHandler();
}
scrollHandler();

window.addEventListener("scroll", scrollHandler);
